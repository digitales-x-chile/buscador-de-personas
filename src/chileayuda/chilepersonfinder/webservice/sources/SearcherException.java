package chileayuda.chilepersonfinder.webservice.sources;

public class SearcherException extends Exception{
	public SearcherException(Throwable throwable){
		super(throwable);
	}
}